# Installation

```
yarn add git+ssh://git@bitbucket.org/topsailtech/ts-flatpickr.git
```

# Usage

In your pack, add
```
import("ts-flatpickr")
```
and reference this pack as a javascript *and* a css resource.

Add to regular Rails date_field using is:
```
<%= f.date_field :date, is: "ts-flatpickr" %>
```

# Options

Add options using the `data: {}` attribute, for example to use time picker
```
<%= f.datetime_local_field :reviewed_at, is: "ts-flatpickr", data: { enable_time: true } %>
```

Full list of options here: https://flatpickr.js.org/options/