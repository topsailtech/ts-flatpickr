import 'flatpickr'

class TsFlatpickr extends HTMLInputElement {
  connectedCallback(){
    if(this.parentElement.classList.contains("flatpickr-wrapper")){
      // For static positioning element gets copied into a wrapper and will be reinitilized. Stop this when nested

    } else {
      flatpickr(this,
        Object.assign(
          {},
          flatpickr_default_opts.bind(this)(),
          this.dataset
        )
      );
    }
  }
};

function flatpickr_default_opts() {
  let flatpickr_opts = {
    altInput: true,
    altFormat: "m/d/Y",
    onClose: function(selectedDates, dateStr, instance) {
      onClose(selectedDates, dateStr, instance)
    },
    static: true
  }

  return flatpickr_opts
}

function onClose(selectedDates, dateStr, instance) {
  if (instance.altInput.value == "") {
    instance.input.value = ""
  }
}

customElements.define('ts-flatpickr', TsFlatpickr, { extends: "input" });